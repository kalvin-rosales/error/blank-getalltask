
const Task = require('./../models/Task')


//Business Logic for Creating a task

/*
1. Add a functtionality to check if there are duplicatte tasks
	- If the task already exists in the database, we return a message `Duplicate Task found`
	- If the task does'nt exist in the database, we add it in the database
2. The task data will be coming from the request's body
3.Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

*/
module.exports.createTask = async(reqBody) => {


// Direct
	// let newTask = new Task({
	// 				name: reqBody.name
	// 			})


	// 			// use save() method to insert the new document in the database


	// 			newTask.save().then((result, err) => {
	// 				//console.log(result)
	// 				if(result){
	// 					return result
	// 				}else{
	// 					return err
	// 				}
	// 			})


	// OR



	return await Task.findOne({name: reqBody.name}).then ( (result, err) => {
		//console.log(result) // a document

		//- If the task already exists in the database, we return a message `Duplicate Task found`

		if(result != null && result.name == reqBody.name){
			return true
		} else {

			//- If the task does'nt exist in the database, we add it in the database
			//Create a new Task object with a "name" field/property
			if(result ==null){




			let newTask = new Task({
				name: reqBody.name
			})
			return newTask.save().then(result => result )

			// use save() method to insert the new document in the database


			// newTask.save().then((result, err) => {
			// 	//console.log(result)
			// 	if(result){
			// 		return result
			// 	}else{
			// 		return err
		// 		}
		// 	})
		}
	}

})
	//GET ALL TASKS

	// Business Logic for getting all the tasks
	/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/ Postman and return an array of doucments
	*/
module.exports.getAllTasks = async () => {
	
	return await Task.find().then((result,err ) => {
		console.log(result) 
	// 	if(result){ 
	// 	return result
	// } else {
	// 	return err
	// }
})

}



//DELETE A TASK
module.exports.deleteTask = async (id) => {


	return await Task.findByIdAndDelete(id).then(result => {
		try{
			if(result != null){
				return true
			} else {
				return false
			}
		} catch (err){
			return err
		}
		
	})
}

//UPDATE A TASK
module.exports.updateTask = async (id, reqBody) => {
	console.log(reqBody)
	return await Task.findByIdAndUpdate(id,{$set:reqBody},{new:true}).then(result => result)
}


}