const mongoose = require('mongoose');


//Create a schema for tasks
	//schema determines the structure of the cdocuments to be written in the database
	// schema acts as a blueprint to our data
const taskSchema = new mongoose.Schema(
	{
		name: {
			type:String,
			required: [true,`Name is required`]
		},
		status: {
			type: String,
			default: "pending"
		}
	}

)


// create a model out of the schema
	//mongoose.model(<name of the model>, < schema  came from>)



module.exports = mongoose.model(`Task`,taskSchema)




	// model is a programming interface that enables us to query and manipulate database using its methods
	//using module.exports, we allow Task model to be used outside of its current module